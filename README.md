package com.soft2.dao;

import java.util.List;

//import org.vosao.dao.impl.CommentEntity;
//import org.vosao.dao.impl.CommentHelper;

import com.mongodb.BasicDBObject;

import com.mongodb.DBObject;

public class ContentDao extends AbstractDao{

	public ContentDao(){
		super();
	}

	public List<DBObject> select(String parent Class,
		Long parentKey) {
		BasicDBObject query = new BasicDBObject();
		query.put("parentClass", parentClass);
		query.put("parentKey", parentKey);
      	 	List<DBObject> result = this.getDbCollection().find(query).toArray();
		return result;		
	}

	public DBObject getByLanguage(String parentClass,
		Long parentKey, String Language) {
		BasicDBObject query = new BasicDBObject();
		query.put("parentClass", parentClass);
		query.put("parentKey", parentKey);
		query.put("languageCode", languageCode);
		return this.getDbCollection().findOne(query);
	}

	public void removeById(String className, Long if) {
		BasicDBObject query = new BasicDBObject();
		query.put("parentClass", parentClass);
		query.put("parentKey", parentKey);
		this.getDbCollection().remove(query);
	}

}